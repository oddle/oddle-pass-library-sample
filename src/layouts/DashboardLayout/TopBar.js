import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { AppBar, Box, Toolbar, Button, CircularProgress, makeStyles, Menu, MenuItem, withStyles } from '@material-ui/core';
import { Home as AcUnitIcon } from '@material-ui/icons';
import ConnectedSection from 'containers/ConnectedSection';
import { storeKeys } from 'reduxStore'
import {addListener, initializeApp, signOut} from '@jarvis.oddle.me/oddle.pass'
import { parseJwt } from 'tools/token'

const useStyles = makeStyles((theme) => ({
  root: {
    zIndex: 10,
    backgroundColor: '#fff',
    borderRadius: '3px'
  },
  avatar: {
    width: 60,
    height: 60
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

const StyledMenu = withStyles({ paper: { border: '1px solid #d3d4d5', }, })((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const TopBar = ({ getCustomerInfoAction, [storeKeys.user]: { loading, data }, loginAction }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogin = async () => {
    async function initOddlePass() {
      await initializeApp(
        '8a818fb676b6d1940176d1acd4e806ca',
        'http://oddleapp.com/oauth-test',
        'en_SG',
        {
          logoUrl: 'https://cdn.oddle.me/wp-content/uploads/2020/09/small-oddle-logo-transparent.png',
          headerColor: 'rgb(10, 48, 64)',
          buttonCloseColor: '#fff',
          menuName: 'My Website',
          state: { urlTest: 'http://localhost:3006/test/haha?test=qsdgfs' }
        })
    }
    initOddlePass().then(() => {
      addListener(async (action) => {
        console.log("================================================");
        console.log("[TopBar.js] action", action);
        console.log("================================================");
        switch (action) {
          case 'facebook_signin_success':
            await getCustomerInfoAction()
            break
          case 'initialized':
            await loginAction()
            await getCustomerInfoAction()
            break
          case 'signin_success':
            const parsedToken = parseJwt(window.localStorage.getItem('accessToken'))
            window.location.href = parsedToken.state.urlTest
            break
          case 'signout_success':
            window.location.reload()
            break
          default: break
        }
      });
    })
  }

  return (
    <AppBar elevation={0} className={classes.root}>
      <Toolbar>
        <RouterLink to="/">
          <AcUnitIcon color={'inherit'}/>
        </RouterLink>
        <Box flexGrow={1}/>
        <div className={classes.wrapper}>
          <Button
            variant="contained"
            color="primary"
            disabled={loading}
            onClick={Object.keys(data || {}).length > 0 ? handleClick : handleLogin}
          >
            {
              Object.keys(data || {}).length > 0 ? `Welcome, ${data.firstName}` : 'Login'
            }
          </Button>
          {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
        {
          Object.keys(data || {}).length > 0 && <StyledMenu
            id="customized-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem>
              <RouterLink to="/account">
                Account Details
              </RouterLink>
            </MenuItem>
            <MenuItem onClick={() => signOut(data.id)}>
              Logout
            </MenuItem>
          </StyledMenu>
        }
      </Toolbar>
    </AppBar>
  );
}

TopBar.propTypes = {
  className: PropTypes.string,
  getCustomerInfoAction: PropTypes.func,
  [storeKeys.user]: PropTypes.object,
};

export default ConnectedSection(TopBar, {
  storeKeysToSubscribe: [storeKeys.user]
});
