import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Card, CardContent, CardMedia, Grid, makeStyles } from '@material-ui/core';

const data = [
  {
    id: '1',
    name: 'One-Pot Spicy and Potatoes',
    imageUrl: 'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=700%2C636',
    updatedAt: moment().subtract(2, 'hours')
  },
  {
    id: '2',
    name: 'Burger',
    imageUrl: 'https://www.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/2KL6JYQYH4I6REYMIWBYVUGXPI.jpg&w=1440',
    updatedAt: moment().subtract(2, 'hours')
  },
  {
    id: '3',
    name: 'Pizza',
    imageUrl: 'https://www.wegmans.com/wp-content/uploads/3961327_full-width-meals-kids-health-2-mobile.jpg',
    updatedAt: moment().subtract(3, 'hours')
  },
  {
    id: '4',
    name: 'Spaghetti',
    imageUrl: 'https://yummy.co.ke/wp-content/uploads/2015/10/15367005414_6c2b5fe86b_k-e1444989857323.jpg',
    updatedAt: moment().subtract(5, 'hours')
  },
  {
    id: '5',
    name: 'Seafood',
    imageUrl: 'https://cdn.cnn.com/cnnnext/dam/assets/170302153529-garlic-crab.jpg',
    updatedAt: moment().subtract(9, 'hours')
  }
];

const useStyles = makeStyles(({
  root: {
    height: '100%'
  },
  image: {
    paddingTop: '80%'
  }
}));

const ListProducts = ({ className, ...rest }) => {
  const classes = useStyles();
  const [products] = useState(data);

  return (
    <Grid container spacing={3}>
      {
        products.map((product, key) => (
          <Grid item xs={6} sm={4} md={3} lg={2} key={key}>
            <Card
              className={clsx(classes.root, className)}
              {...rest}
            >
              <CardMedia image={product.imageUrl} className={classes.image}/>
              <CardContent>{product.name}</CardContent>
            </Card>
          </Grid>
        ))
      }
    </Grid>
  );
};

ListProducts.propTypes = {
  className: PropTypes.string
};

export default ListProducts;
