import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'containers/Page';
import ListProducts from './ListProducts';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const Dashboard = () => {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Dashboard"
    >
      <Container maxWidth={false}>
        <ListProducts />
      </Container>
    </Page>
  );
};

export default Dashboard;
