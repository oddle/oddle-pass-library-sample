import React from 'react';
import { Box, Button, Card, CardContent, CardHeader, Divider, Grid, TextField, Snackbar, withStyles, SnackbarContent } from '@material-ui/core';
import { Field, reduxForm } from 'redux-form';
import { storeKeys } from 'reduxStore';
import ConnectedSection from 'containers/ConnectedSection';
import { changePassword } from '@jarvis.oddle.me/oddle.pass'

const styled = {
  true: {
    backgroundColor: '#4caf50',
    color: '#fff'
  },
  false: {
    backgroundColor: '#f44336',
    color: '#fff'
  },
  button: {
    marginLeft: '20px'
  }
}

const renderTextField = field => <TextField
  fullWidth
  variant={'outlined'}
  hintText={field.label}
  label={field.label}
  errorText={field.touched && field.error}
  {...field.input}
/>

let ProfileDetails = ({ initialize, initialValues, updateCustomerDetailsAction, classes, handleSubmit }) => {
  const { email, id } = initialValues
  const [openSnackbar, setOpenSnackbar] = React.useState(null)
  React.useEffect(() => {initialize(initialValues)}, [initialize, initialValues])

  const onSubmit = async (values) => {
    const response = await updateCustomerDetailsAction(values)
    if (response) {
      setOpenSnackbar(true)
    } else{
      setOpenSnackbar(false)
    }
  }

  const handleCloseSnackbar = () => {
    setOpenSnackbar(null)
  }

  return (
    <div>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openSnackbar !== null}
        onClose={handleCloseSnackbar}
        autoHideDuration={3000}
        className={classes[openSnackbar]}
      >
        <SnackbarContent className={classes[openSnackbar]} message={openSnackbar ? 'Update success' : 'Update fail'} />
      </Snackbar>
      <form onSubmit={handleSubmit(onSubmit)} >
        <Card>
          <CardHeader subheader="The information can be edited" title="Profile"/>
          <Divider/>
          <CardContent>
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <Field label="First name" name={'firstName'} component={renderTextField}/>
              </Grid>
              <Grid item md={6} xs={12}>
                <Field label="Last name" name={'lastName'} component={renderTextField}/>
              </Grid>
              <Grid item md={6} xs={12}>
                <Field label="Phone" name={'phone'} component={renderTextField}/>
              </Grid>
              <Grid item md={6} xs={12}>
                <Field label="Email" name={'email'} component={renderTextField}/>
              </Grid>
            </Grid>
          </CardContent>
          <Divider/>
          <Box display="flex" justifyContent="flex-end" p={2}>
            <Button type="button" color="secondary" variant="contained" onClick={() => changePassword(email, id)}>
              Reset password
            </Button>
            <Button type="submit" color="primary" variant="contained" className={classes.button}>
              Save details
            </Button>
          </Box>
        </Card>
      </form>
    </div>
  );
}

// Decorate with reduxForm(). It will read the initialValues prop provided by connect()
ProfileDetails = reduxForm({
  form: 'ProfileDetails'  // a unique identifier for this form
})(ProfileDetails)

// You have to connect() to any reducers that you wish to connect to yourself
ProfileDetails = ConnectedSection(ProfileDetails, {
  storeKeysToSubscribe: [storeKeys.user]
})

export default withStyles(styled)(ProfileDetails)
