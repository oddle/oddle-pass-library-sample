import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Avatar, Box, Card, CardContent, Divider, makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    height: 100,
    width: 100
  }
}));

const Profile = ({ className, data, ...rest }) => {
  const classes = useStyles();

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <CardContent>
        <Box alignItems="center" display="flex" flexDirection="column">
          <Avatar className={classes.avatar}/>
          <Typography color="textPrimary" gutterBottom variant="h3">
            {`${data.firstName} ${data.lastName}`}
          </Typography>
          <Typography color="textSecondary" variant="body1">
            {data.email}
          </Typography>
        </Box>
      </CardContent>
      <Divider/>
    </Card>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
