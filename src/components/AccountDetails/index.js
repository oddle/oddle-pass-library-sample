import React from 'react';
import { Container, Grid, makeStyles } from '@material-ui/core';
import Page from 'containers/Page';
import Profile from './Profile';
import ProfileDetails from './ProfileDetails';
import ConnectedSection from "containers/ConnectedSection";
import { storeKeys } from "reduxStore";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const Account = ({ [storeKeys.user]: { data } }) => {
  const classes = useStyles();

  return (
    <Page className={classes.root} title="Account">
      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Grid item lg={4} md={6} xs={12}>
            <Profile data={data}/>
          </Grid>
          <Grid item lg={8} md={6} xs={12}>
            <ProfileDetails initialValues={data}/>
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default ConnectedSection(Account, {
  storeKeysToSubscribe: [storeKeys.user]
});
