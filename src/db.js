import Dexie from 'dexie';

const db = new Dexie('OddlePassLibrarySample');
db.version(1).stores({ user: '++id' });

export default db;
