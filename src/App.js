import React from 'react';
import { ThemeProvider, createStyles, makeStyles } from '@material-ui/core';
import theme from 'theme';
import { Route, Switch } from 'react-router-dom';
import DashboardLayout from "layouts/DashboardLayout";
import { initializeApp, addListener } from '@jarvis.oddle.me/oddle.pass'
import ConnectedSection from "containers/ConnectedSection";
import { storeKeys } from "reduxStore";
import Home from 'components/Dashboard'
import AccountDetails from 'components/AccountDetails'
import { parseJwt } from 'tools/token'

const useStyles = makeStyles(() => createStyles({
  '@global': {
    '*': {
      boxSizing: 'border-box',
      margin: 0,
      padding: 0,
    },
    html: {
      '-webkit-font-smoothing': 'antialiased',
      '-moz-osx-font-smoothing': 'grayscale',
      height: '100%',
      width: '100%'
    },
    body: {
      backgroundColor: '#f4f6f8',
      height: '100%',
      width: '100%'
    },
    a: {
      textDecoration: 'none'
    },
    '#root': {
      height: '100%',
      width: '100%'
    }
  }
}));

const GlobalStyles = () => {
  useStyles();

  return null;
};

function PrivateRoute ({component: Component, authed, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => authed === true ? <Component {...props} /> : <Home />}
    />
  )
}

const App = (props) => {
  const { [storeKeys.user]: { data }, getCustomerInfoAction } = props
  const [initializing, setInitializing] = React.useState(true)

  React.useEffect(() => {
    async function initOddlePass() {
      await initializeApp(
        '8a818fb676b6d1940176d1acd4e806ca',
        'http://oddleapp.com/oauth-test',
        'en_SG',
        {
          logoUrl: 'https://cdn.oddle.me/wp-content/uploads/2020/09/small-oddle-logo-transparent.png',
          headerColor: 'rgb(10, 48, 64)',
          buttonCloseColor: '#fff',
          menuName: 'My Website'
        })
    }
    initOddlePass().then(() => {
      addListener(async (action) => {
        console.log("================================================");
        console.log("[App.js] action", action);
        console.log("================================================");
        switch (action) {
          case 'facebook_signin_success':
            await getCustomerInfoAction()
            break
          case 'initialized':
            await getCustomerInfoAction()
            break
          case 'signin_success':
            const parsedToken = parseJwt(window.localStorage.getItem('accessToken'))
            window.location.href = parsedToken.state.urlTest
            break
          case 'signout_success':
            window.location.reload()
            break
          default: break
        }
      });
      setInitializing(false)
    })
  }, [getCustomerInfoAction])

  if (initializing) {
    return <div/>
  }

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <DashboardLayout>
        <Switch>
          <PrivateRoute exact path="/account" component={AccountDetails} authed={!!(data && data.id)}/>
          <Route exact path="/" component={Home} />
          <Route exact path="/test/haha" component={Home} />
        </Switch>
      </DashboardLayout>
    </ThemeProvider>
  );
};

export default ConnectedSection(App, {
  storeKeysToSubscribe: [storeKeys.user]
});
