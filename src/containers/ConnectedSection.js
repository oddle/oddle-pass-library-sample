import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { actions } from 'reduxStore'

const getReduxStoreKeys = (storeKeysToSubscribe, props) => typeof storeKeysToSubscribe === 'function' ? storeKeysToSubscribe(props) : storeKeysToSubscribe

const makeConnectedSection = (Component, configs) => {
  const { storeKeysToSubscribe = [], initialValuesForm } = configs
  const mapStateToProps = (state, ownProps) => {
    const reduxStoreKeys = getReduxStoreKeys(storeKeysToSubscribe, ownProps)
    const props = reduxStoreKeys.reduce((acc, storeKey) => ({ ...acc, [storeKey]: state[storeKey] }), {})
    if (!!initialValuesForm) {
      ownProps.initialValues = props[initialValuesForm].data
    }
    return { ...ownProps, ...props }
  }
  const mapDispatchToProps = (dispatch, ownProps) => {
    const reduxStoreKeys = getReduxStoreKeys(storeKeysToSubscribe, ownProps)
    return bindActionCreators(reduxStoreKeys.reduce((acc, storeKey) => ({ ...acc, ...actions[storeKey] }), {}), dispatch)
  }
  const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(Component)
  ConnectedComponent.storeKeysToSubscribe = storeKeysToSubscribe
  return ConnectedComponent
}

export default makeConnectedSection
