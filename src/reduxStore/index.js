import { combineReducers, createStore, applyMiddleware } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'

import user from './user'

const combineSubStores = (subStores) => {
  const extractField = (key) => Object.keys(subStores).reduce((result, store) => ({ ...result, [subStores[store].storeKey]: subStores[store][key] }), {})
  const actions = extractField('actions')
  const reducers = combineReducers({ ...extractField('reducers'), form: formReducer })
  const initialState = extractField('initialState')
  const storeKeys = Object.keys(subStores).reduce((result, store) => ({ ...result, [store]: subStores[store].storeKey }), {})
  return { actions, reducers, initialState, storeKeys }
}

export const { actions, reducers, initialState, storeKeys, initialiseState } = combineSubStores({
  user
})

export default createStore(reducers, initialiseState, composeWithDevTools(applyMiddleware(thunkMiddleware)))
