import * as constant from './constants';

export const initialState = {
  fetched: false,
  loading: false,
  message: null,
  data: {}
}

export function reducers(state = initialState, action) {
  switch (action.type) {
    case constant.LOGIN_START:
    case constant.SET_USER_DATA_START:
      return {
        ...state,
        loading: true,
        fetched: true
      };
    case constant.LOGIN_SUCCESS:
    case constant.SET_USER_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload,
        fetched: true
      };
    case constant.LOGIN_ERROR:
    case constant.SET_USER_DATA_ERROR:
      return {
        ...state,
        loading: false,
        data: [],
        message: action.message,
        fetched: true
      };
    default:
      return state;
  }
}
