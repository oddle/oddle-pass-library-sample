import { STORE_KEY } from './constants'
import * as actions from './actions'
import { initialState, reducers } from './reducers'

export default { actions, reducers, initialState, storeKey: STORE_KEY }
