export const STORE_KEY = '@@user'

// Login
export const LOGIN_START = 'LOGIN_START';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

// Load User Account Data
export const SET_USER_DATA_START = 'SET_USER_DATA_START';
export const SET_USER_DATA_SUCCESS = 'SET_USER_DATA_SUCCESS';
export const SET_USER_DATA_ERROR = 'SET_USER_DATA_ERROR';

// Verification details for account have logged in at another site
export const VERIFICATION_DETAILS_START = 'VERIFICATION_DETAILS_START';
export const VERIFICATION_DETAILS_SUCCESS = 'VERIFICATION_DETAILS_SUCCESS';
export const VERIFICATION_DETAILS_ERROR = 'VERIFICATION_DETAILS_ERROR';

// Verification details for account have logged in at another site
export const UPDATE_CUSTOMER_START = 'UPDATE_CUSTOMER_START';
export const UPDATE_CUSTOMER_SUCCESS = 'UPDATE_CUSTOMER_SUCCESS';
export const UPDATE_CUSTOMER_ERROR = 'UPDATE_CUSTOMER_ERROR';
