import * as constants from './constants'
import { signIn, getCustomerInfo, updateCustomerDetails } from '@jarvis.oddle.me/oddle.pass'
import db from 'db'
import { menuId } from 'config'

function storeUser(data) {
  return db.table('user').add({
    oddlePassId: data.id,
    phone: data.phone,
    email: data.email,
    gender: data.gender,
    lastName: data.lastName,
    firstName: data.firstName,
    facebookId: data.facebookId,
    acceptMarketing: data.acceptMarketing,
    acceptMarketingMenu: data.acceptMarketingMenu,
  })
}

export function loginAction() {
  return async (dispatch) => {
    dispatch({ type: constants.LOGIN_START });

    try {
      const { data, success, message } = await signIn(menuId);

      if (success && data.action === 'signup') {
        await storeUser(data)
        await dispatch(getCustomerInfoAction())
      }

      dispatch({
        type: success ? constants.LOGIN_SUCCESS : constants.LOGIN_ERROR,
        message: message,
        data
      });
    } catch (error) {
      dispatch({
        type: constants.LOGIN_ERROR,
        message: 'Cannot login',
      });
      return false;
    }

    return true;
  };
}

export function getCustomerInfoAction() {
  return async (dispatch) => {
    dispatch({ type: constants.SET_USER_DATA_START });

    try {
      if (window.localStorage.getItem('accessToken')) {
        const { data, success, message } = await getCustomerInfo();

        dispatch({
          type: success ? constants.SET_USER_DATA_SUCCESS : constants.SET_USER_DATA_ERROR,
          message: message,
          payload: data
        });
      } else {
        dispatch({
          type: constants.SET_USER_DATA_SUCCESS,
          payload: {}
        });
      }
    } catch (error) {
      dispatch({
        type: constants.SET_USER_DATA_ERROR,
        message: error,
      });
    }
  }
}

export function updateCustomerDetailsAction(values) {
  return async (dispatch) => {
    dispatch({ type: constants.UPDATE_CUSTOMER_START });

    try {
      const { data, success, message } = await updateCustomerDetails(values.id, {
        firstName: values.firstName,
        lastName: values.lastName,
        phone: values.phone,
        email: values.email,
      });

      if (success) {
        await storeUser(data)
        await dispatch(getCustomerInfoAction())
      }

      dispatch({
        type: success ? constants.UPDATE_CUSTOMER_SUCCESS : constants.UPDATE_CUSTOMER_ERROR,
        message: message,
        payload: data
      });
      return { data, success, message }
    } catch (error) {
      dispatch({
        type: constants.UPDATE_CUSTOMER_ERROR,
        message: error,
      });
    }
  }
}
